var HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

const config = {
  entry: './src/script/app.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  watch: true,
  devServer: {
    contentBase: path.join(__dirname, "../dist/"),
    compress: true,
    port: 3000
  },
  module: {
    rules: [{
      test: /\.(s*)css$/,
      use: ['style-loader', 'css-loader', 'sass-loader']
    }]
  },
  plugins: [
    new HtmlWebpackPlugin({
      hash: true,
      title: 'Webpack Boileplate',
      template: './src/index.html',
      filename: './index.html' //relative to root of the application
    })
  ]
};

module.exports = config;